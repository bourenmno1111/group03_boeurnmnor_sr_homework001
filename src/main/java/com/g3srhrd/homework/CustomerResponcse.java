package com.g3srhrd.homework;

import java.time.LocalDateTime;
import java.util.AbstractList;

public class CustomerResponcse<A> {
    private LocalDateTime dateTime;
    private int stutus;
    private String message;
    private A result;

    public A getResult() {
        return result;
    }

    public void setResult(A result) {
        this.result = result;
    }

    public CustomerResponcse(LocalDateTime dateTime, int stutus, String message, A result) {
        this.dateTime = dateTime;
        this.stutus = stutus;
        this.message = message;
        this.result = result;
    }

    public CustomerResponcse(LocalDateTime dateTime, int stutus, String message) {
        this.dateTime = dateTime;
        this.stutus = stutus;
        this.message = message;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public int getStutus() {
        return stutus;
    }

    public void setStutus(int stutus) {
        this.stutus = stutus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
