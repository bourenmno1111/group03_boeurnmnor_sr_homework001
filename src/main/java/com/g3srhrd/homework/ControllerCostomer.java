package com.g3srhrd.homework;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class ControllerCostomer {
    ArrayList<Customer> customersList = new ArrayList<>();
    int id =1;
    public ControllerCostomer(){

        customersList.add(new Customer(id++,"Mnor","M",20,"Phnome penh"));
        customersList.add(new Customer(id++,"Jorn","M",22,"Takeo"));
        customersList.add(new Customer(id++,"smit","M",23,"Phmom penh"));
        customersList.add(new Customer(id++,"seavlang","s",16,"Btb"));
    }
    @PostMapping("api/v1/Customer")
    public Customer intsert(@RequestBody CustomerRequest customerRequest){
        Customer customers = new Customer();
        customers.setId(id++);
        customers.setName(customerRequest.getName());
        customers.setGender(customerRequest.getGender());
        customers.setAge(customerRequest.getAge());
        customers.setAdrress(customerRequest.getAddress());
        customersList.add(customers);
    return customers;
    }
    @GetMapping("api/v1/Customer")
    public ResponseEntity<?> getCustomers(){
     return ResponseEntity.ok(new CustomerResponcse<ArrayList<Customer>>(
             LocalDateTime.now(),
             200,
             "Successfully",
             customersList


     ));
//        return customersList;
    }
    @GetMapping("api/v1/Customer/{id}")
    public ResponseEntity<?> getcoustomerByid(@PathVariable("id") int customerid){
        for(Customer i:customersList){
            if (i.getId() == customerid){
                return ResponseEntity.ok(new CustomerResponcse<Customer>(
                        LocalDateTime.now(),
                        200,
                        "Successfully",
                                i
                        ));
            }
        }
//        System.out.println("hello");

        return null;
    }
    @GetMapping("api/v1/Customer/search")
    public ResponseEntity getcoustomerByName(@RequestParam String name){
        for(Customer i: customersList) {
            if (i.getName().equals(name)) {
                return ResponseEntity.ok(new CustomerResponcse<Customer>(
                        LocalDateTime.now(),
                        200,
                        "Successfully",
                        i
                ));
            }
        }
        return null;
    }

    @PutMapping("api/v1/Customer/{customerid}")
    public ResponseEntity getcoustomerByid(@PathVariable int customerid, @Valid @RequestBody CustomerRequest customerRequest){
        for(Customer i:customersList){
            if (i.getId() == customerid){
                i.setName(customerRequest.getName());
                i.setGender(customerRequest.getGender());
                i.setAge(customerRequest.getAge());
                i.setAdrress(customerRequest.getAddress());
//                customersList.set((id-1),i);
                return ResponseEntity.ok(new CustomerResponcse<Customer>(
                        LocalDateTime.now(),
                        200,
                        "Successfully",
                        i
                ));
            }
        }
//        System.out.println("hello");

        return null;
    }
    @DeleteMapping("api/v1/Customer/{id}")
    public ResponseEntity DeleteByid(@PathVariable("id") int customerid){
        for(Customer i:customersList){
            if (i.getId() == customerid){
               customersList.remove(i);
                return ResponseEntity.ok(new CustomerResponcse<Customer>(
                        LocalDateTime.now(),
                        200,
                        " Delete Successfully",
                        i
                ));
            }
        }
//        System.out.println("hello");

        return null;
    }

}
