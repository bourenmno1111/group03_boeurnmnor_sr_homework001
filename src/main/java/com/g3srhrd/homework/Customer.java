package com.g3srhrd.homework;

public class Customer {
    private int id;
    private String name;
    private String gender;
    private  int age;
    private String adrress;

    public Customer(int id, String name, String gender, int age, String adrress) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.adrress = adrress;
    }

    public Customer(String name, String gender, int age, String adrress) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.adrress = adrress;
    }

    public Customer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAdrress() {
        return adrress;
    }

    public void setAdrress(String adrress) {
        this.adrress = adrress;
    }
}
